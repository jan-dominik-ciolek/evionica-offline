const path = require('path');
const express = require('express');



const app = express();
const publicPath = path.join(__dirname, 'build');

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

let planeCords = {
    lat: 52.22, lng: 21.02
}

app.use(express.static(publicPath));

app.get('*', (req, res) => {
  res.sendFile(path.join(publicPath, 'index.html'));
});

app.listen(3000, () => {
  console.log('Server is up!');
});