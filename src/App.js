import logo from './logo.svg';
import './App.css';
import 'leaflet/dist/leaflet.css'
import FlyingBoard from './components/FlyingBoard'

function App() {
  return (
    <div className="App">
        <FlyingBoard />
    </div>
  );
}

export default App;
