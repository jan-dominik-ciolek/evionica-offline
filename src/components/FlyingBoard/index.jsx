import { MapContainer, TileLayer } from 'react-leaflet'
import './index.css'

import Plane from './components/Plane'

const position = [52, 21]

const FlyingBoard = () => {
    // return null
    return (



        <MapContainer className='map-container-custom' center={position} zoom={5}>
            <TileLayer
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <Plane center={{ lat: 52.22, lng: 21.02 }} />

        </MapContainer>


    );
}

export default FlyingBoard;