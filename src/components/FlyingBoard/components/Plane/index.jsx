import React, { useState, useRef, useMemo, useCallback } from 'react'
import plane from './plane.svg';
import { Marker, Popup } from 'react-leaflet'
import { icon } from 'leaflet'

import './index.css'

const markerIcon = icon({
    iconSize: [38, 95],
    iconUrl:plane,
})

const Plane = ({center}) => {
    const [draggable, setDraggable] = useState(false)
    const [position, setPosition] = useState(center)
    const markerRef = useRef(null)
    const eventHandlers = useMemo(
        () => ({
            dragend() {
                const marker = markerRef.current
                if (marker != null) {
                    setPosition(marker.getLatLng())
                }
            },
        }),
        [],
    )
    const toggleDraggable = useCallback(() => {
        setDraggable((d) => !d)
    }, [])


    return (
        <Marker
            icon={markerIcon}
            draggable={true}
            eventHandlers={eventHandlers}
            position={position}
            ref={markerRef}>
            <div className="plane-cords">Cords:</div>
        </Marker>

    );
}

export default Plane;